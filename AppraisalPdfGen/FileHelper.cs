﻿using System;
using System.Linq;
using System.IO;

namespace AppraisalPdfGen
{
    class FileHelper
    {
        internal static bool PathExists(string path)
        {
            try
            {
                if (!File.Exists(path))
                {
                    Console.WriteLine("File does not exist");
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return true;
        }
        internal static bool CheckExt(string fname, params string[] extList)
        {
            string ext = Path.GetExtension(fname).ToLower();
            if (extList.Where(e => e == ext).Count() == 0)
            {
                Console.WriteLine(string.Join(" or ", extList) + " file needed as input");
                return false;
            }
            return true;
        }
    }
}
