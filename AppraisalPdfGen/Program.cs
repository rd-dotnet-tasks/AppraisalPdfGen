﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using ExcelDataReader;
using System.Data;
using Word = Microsoft.Office.Interop.Word;

namespace AppraisalPdfGen
{
    class Program
    {
        private static String outDir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName + "Out/";
        private static Word.Application wordApp = new Word.Application();
        private static List<string> validColNames = new List<string>() { "Name", "Email" };

        private static void ProcessDoc(IDictionary<string, string> excelData)
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("\nEnter path to word template");
                    string wordPath = Console.ReadLine();
                    if (!FileHelper.PathExists(wordPath))
                        continue;
                    if (!FileHelper.CheckExt(wordPath, ".doc", ".docx"))
                        continue;
                    foreach (KeyValuePair<string, string> kv in excelData)
                    {
                        byte[] byteArray = File.ReadAllBytes(wordPath);
                        string email = kv.Key;
                        string name = kv.Value;
                        using (MemoryStream stream = new MemoryStream())
                        {
                            stream.Write(byteArray, 0, (int)byteArray.Length);
                            using (WordprocessingDocument doc =
                            WordprocessingDocument.Open(stream, true))
                            {
                                const string FieldDelimeter = " MERGEFIELD ";
                                foreach (FieldCode field in doc.MainDocumentPart.RootElement.Descendants<FieldCode>())
                                {
                                    var fieldNameStart = field.Text.LastIndexOf(FieldDelimeter, StringComparison.Ordinal);
                                    var fieldName = field.Text.Substring(fieldNameStart + FieldDelimeter.Length).Trim();

                                    foreach (Run run in doc.MainDocumentPart.Document.Descendants<Run>())
                                    {
                                        foreach (Text txtFromRun in run.Descendants<Text>().Where(a => a.Text == "«" + fieldName + "»"))
                                        {
                                            if (fieldName == "First_Name")
                                                txtFromRun.Text = name;
                                            // for other merge rules
                                        }
                                    }
                                }
                            }
                            string fileOutDir = outDir + name;
                            string docOutDir = fileOutDir + ".docx";
                            File.WriteAllBytes(docOutDir, stream.ToArray());
                            SaveDocAsPDF(fileOutDir, docOutDir);
                        }
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.ReadKey();
                    Environment.Exit(1);
                }
            }
        }

        private static void SaveDocAsPDF(string fileOutDir, string docOutDir)
        {
            Word.Document wordDoc = null;
            try
            {
                wordDoc = wordApp.Documents.Open(docOutDir, ReadOnly: true);
                wordDoc.ExportAsFixedFormat(fileOutDir + ".pdf", Word.WdExportFormat.wdExportFormatPDF);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                wordDoc.Close(false);
                Marshal.ReleaseComObject(wordDoc);
                File.Delete(docOutDir);
            }
        }

        private static IDictionary<string, string> ReadFromExcel()
        {
            IDictionary<string, string> excelData = new Dictionary<string, string>();

            while (true)
            {
                try
                {
                    Console.WriteLine("\nEnter path to excel file");
                    string xlPath = Console.ReadLine();
                    if (!FileHelper.PathExists(xlPath))
                        continue;
                    if (!FileHelper.CheckExt(xlPath, ".xls", ".xlsx"))
                        continue;
                    using (var stream = File.Open(xlPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                {
                                    UseHeaderRow = true
                                }
                            });
                            var sheet = result.Tables[0];
                            foreach (DataRow row in sheet.Rows)
                            {
                                string nameValue = null;
                                foreach (string colName in validColNames)
                                {
                                    var val = row[sheet.Columns[colName]];
                                    if (nameValue is null)
                                    {
                                        if (val.GetType() == typeof(string))
                                        {
                                            if (val.ToString() != "")
                                                nameValue = val.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (val.GetType() == typeof(string))
                                        {
                                            if (val.ToString() != "")
                                                excelData[val.ToString()] = nameValue;
                                        }
                                    }
                                }
                            }
                        }
                        // data has been successfully read into the set, so break out of while
                        break;
                    }
                }
                catch (Exception e)
                {
                    if (e.Message == "continue")
                        continue;

                    // handle wrong column name
                    else if(e.TargetSite.Name == "CheckColumn")
                    {
                        Console.WriteLine("Valid Headers not found. Use (" + String.Join(", ", validColNames.ToArray()) + ") as Header Names.");
                        continue;
                    }
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    Environment.Exit(1);
                }
            }
            return excelData;
        }

        static void Main(string[] args)
        {
            try
            {
                Directory.CreateDirectory(outDir);
                while (true)
                {
                    IDictionary<string, string> excelData = ReadFromExcel();
                    ProcessDoc(excelData);
                }
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                wordApp.Quit(false);
                Marshal.ReleaseComObject(wordApp);
            }
        }
    }
}